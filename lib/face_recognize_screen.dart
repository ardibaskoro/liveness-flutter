// ignore_for_file: use_build_context_synchronously

import 'dart:async';

import 'package:camera/camera.dart';
import 'package:face_mlkit/models/feature_model.dart';
import 'package:face_mlkit/utilities/camera_helper.dart';
import 'package:face_mlkit/utilities/face_recognize.dart';
import 'package:flutter/material.dart';
import 'package:toastification/toastification.dart';

class FaceRecognizeScreen extends StatefulWidget {
  const FaceRecognizeScreen({
    super.key,
    required this.callback,
  });

  final void Function(String data) callback;

  @override
  State<FaceRecognizeScreen> createState() => _FaceRecognizeScreenState();
}

class _FaceRecognizeScreenState extends State<FaceRecognizeScreen> {
  final cameraController = ValueNotifier<CameraController?>(null);
  final listCameraController = ValueNotifier<List<CameraDescription>>([]);

  final faceRecognize = FaceRecognize();
  final cameraHelper = CameraHelper();

  @override
  void initState() {
    super.initState();
    initializeCamera();
  }

  @override
  void dispose() {
    super.dispose();
    faceRecognize.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Scan Face',
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: Stack(
        clipBehavior: Clip.hardEdge,
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.red,
          ),
          if (cameraController.value != null)
            Center(
              child: Transform.scale(
                scale: 1.4,
                child: CameraPreview(cameraController.value!),
              ),
            ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Colors.white.withOpacity(.3),
            ),
            padding: const EdgeInsets.all(12),
            margin: const EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                for (final permission in features)
                  Row(
                    children: [
                      Icon(
                        listCompletedFeature
                                .where((element) =>
                                    element.featureName ==
                                    permission.featureName)
                                .isNotEmpty
                            ? Icons.check_box
                            : Icons.check_box_outline_blank,
                        color: Colors.green,
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Text(permission.featureName),
                    ],
                  )
              ],
            ),
          )
        ],
      ),
    );
  }

  final listCompletedFeature = <FeatureModel>[];

  Future<void> initializeCamera() async {
    try {
      cameraController.value = await cameraHelper.initialize();
      faceRecognize.initialize(
        cameraController.value,
        // on all feature completed
        onAllFeaturesCompleted,
        // on single feature completed
        onSingleFeatureCompleted,
      );
      setState(() {});
    } catch (err) {
      toastification.show(
        context: context,
        type: ToastificationType.info,
        borderRadius: BorderRadius.circular(12),
        style: ToastificationStyle.flatColored,
        title: Text(err.toString()),
        autoCloseDuration: const Duration(seconds: 1),
      );
    }
  }

  void onAllFeaturesCompleted(String? path) {
    if (path != null) {
      widget.callback(path);
    }
  }

  void onSingleFeatureCompleted(FeatureModel feature) {
    if (listCompletedFeature
        .where((element) => element.featureName == feature.featureName)
        .isEmpty) {
      listCompletedFeature.add(feature);
      setState(() {});
    }
  }
}
