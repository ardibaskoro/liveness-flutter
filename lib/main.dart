import 'dart:io';

import 'package:face_mlkit/face_recognize_screen.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:toastification/toastification.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String imagePicture = '';

  @override
  void initState() {
    super.initState();
    Permission.camera.request();
    Permission.microphone.request();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (imagePicture.isNotEmpty) Image.file(File(imagePicture)),
            const SizedBox(height: 24),
            MaterialButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => FaceRecognizeScreen(
                      callback: (value) {
                        Navigator.pop(context);
                        setState(() {
                          imagePicture = value;
                        });
                        toastification.show(
                          context: context,
                          title: Text(
                            'Berhasil scan foto',
                          ),
                          autoCloseDuration: const Duration(seconds: 3),
                        );
                      },
                    ),
                  ),
                );
              },
              child: Text('Log in'),
            ),
            MaterialButton(
              onPressed: () {
                setState(() {
                  imagePicture = '';
                });
              },
              child: Text('Reset'),
            ),
          ],
        ),
      ),
    );
  }
}
