import 'dart:async';
import 'dart:developer';

import 'package:camera/camera.dart';
import 'package:face_mlkit/models/feature_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';

class FaceRecognize {
  final _options = FaceDetectorOptions(
    enableLandmarks: true,
    enableTracking: true,
    enableClassification: true,
    enableContours: true,
    performanceMode: FaceDetectorMode.fast,
  );

  FaceDetector get faceDetector {
    return FaceDetector(
      options: _options,
    );
  }

  Future<void> initialize(
    CameraController? controller,
    void Function(String? path) onAllFeatureCompleted,
    void Function(FeatureModel feature) onFeatureCompleted,
  ) async {
    if (controller == null) {
      throw PlatformException(
        code: '000',
        message: 'Camera Controller failed to initialize',
      );
    }

    _periodic = _streamPeriodic.listen((int count) {
      runImageDetection(controller, onFeatureCompleted);
    });

    _featureCompleted.addListener(() {
      if (_featureCompleted.value == features.length) {
        dispose();
        onAllFeatureCompleted(_image.value);
      }
    });
  }

  Future<void> runImageDetection(
    CameraController controller,
    void Function(FeatureModel feature) onFeatureCompleted,
  ) async {
    controller.setFlashMode(FlashMode.off);
    controller.setExposureMode(ExposureMode.locked);

    log('Take Picture');
    final photo = await controller.takePicture();
    final getPhotoDirectory = photo.path;

    log('Input image');
    final inputImage = InputImage.fromFilePath(getPhotoDirectory);

    log('Initialize face detector');

    log('Set image for processing');
    final List<Face> faces = await faceDetector.processImage(inputImage);

    if (faces.isEmpty) {
      throw Exception('No Image Detected');
    } else {
      log('Run logic');
      for (final face in faces) {
        for (final feature in features) {
          feature.recognize(face).then((value) {
            if (value) {
              _featureCompleted.value += 1;
              _image.value = getPhotoDirectory;
              onFeatureCompleted(feature);
            }
          });
        }
      }
    }
  }

  void dispose() {
    faceDetector.close();
    _periodic?.cancel();
  }

  final Stream<int> _streamPeriodic =
      Stream.periodic(const Duration(seconds: 1), (count) => count);

  StreamSubscription<int>? _periodic;

  final _featureCompleted = ValueNotifier<int>(0);
  final _image = ValueNotifier<String>('');
}
