import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/services.dart';

class CameraHelper {
  List<CameraDescription> cameras = [];
  late CameraController controller;

  Future<CameraController> initialize() async {
    if (cameras.isEmpty) {
      cameras = await availableCameras();
    }

    if (cameras.isEmpty) {
      throw PlatformException(code: 'error', message: 'Platform not supported');
    }

    controller = CameraController(
      cameras.last,
      ResolutionPreset.medium,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );

    await controller.initialize().onError((error, stackTrace) {
      throw PlatformException(
        code: error.hashCode.toString(),
        message: error.toString(),
      );
    });

    return controller;
  }

  void dispose() {
    controller.dispose();
  }
}
