import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';

abstract class FeatureModel {
  String get featureName;

  Future<bool> recognize(Face face);
}

class SmilingFeatures extends FeatureModel {
  @override
  String get featureName => 'Smiling';

  @override
  Future<bool> recognize(Face face) async {
    return face.smilingProbability != null && face.smilingProbability! >= .5;
  }
}

class CloseEyeFeatures extends FeatureModel {
  @override
  String get featureName => 'Eye Close';

  @override
  Future<bool> recognize(Face face) async {
    return face.rightEyeOpenProbability != null &&
        face.rightEyeOpenProbability! <= .1 &&
        face.leftEyeOpenProbability != null &&
        face.leftEyeOpenProbability! <= .1;
  }
}

class FeaturePermission {
  final FeatureModel feature;
  final bool isCompleted;

  FeaturePermission({
    required this.feature,
    this.isCompleted = false,
  });

  FeaturePermission copyWith({bool? isCompleted}) => FeaturePermission(
        feature: feature,
        isCompleted: isCompleted ?? this.isCompleted,
      );

  Future<bool> run(Face face) async =>
      isCompleted ? true : await feature.recognize(face);
}

List<FeatureModel> features = [
  SmilingFeatures(),
  CloseEyeFeatures(),
];

